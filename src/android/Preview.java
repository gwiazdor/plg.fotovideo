package pl.appivity.fotovideoplugin;

import android.hardware.Camera;
import android.media.MediaRecorder;
import android.view.View;

import java.io.IOException;

interface Preview {

    PreviewType getPreviewType();

    void setOpacity(float opacity);

    void attach(Camera camera) throws IOException;

    void attach(MediaRecorder recorder);

    View getView();
}

