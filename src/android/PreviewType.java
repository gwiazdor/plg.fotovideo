package pl.appivity.fotovideoplugin;

public enum PreviewType {
    NONE,
    SURFACE,
    TEXTURE_VIEW
}
