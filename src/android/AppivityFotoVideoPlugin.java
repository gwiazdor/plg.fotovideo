package pl.appivity.fotovideoplugin;

import android.view.WindowManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.IOException;


public class AppivityFotoVideoPlugin extends CordovaPlugin {
    private static final String TAG = "APPIVITY_FOTO_VIDEO_PLUGIN";
    private static final String ACTION_PREVIEW = "PREVIEW";
    private static final String ACTION_VIDEO_START = "VIDEO_START";
    private static final String ACTION_VIDEO_STOP = "VIDEO_STOP";
    private static final String ACTION_FOTO = "FOTO";
    private static final String FILE_EXTENSION = ".mp4";
    private static final String IMAGE_FILE_EXTENSION = ".jpg";

    public static final String CAMERA_FRONT = "CAMERA_FRONT";
    public static final String CAMERA_BACK = "CAMERA_BACK";

    private String filePath = "";
    private String fileName = "";
    
    private VideoOverlay videoOverlay;
    private RelativeLayout relativeLayout;

    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        filePath = cordova.getActivity().getFilesDir().toString() + "/";
    }


    @Override
    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
        try {
            Log.d(TAG, "Action: " + action);

            if (AppivityFotoVideoPlugin.ACTION_PREVIEW.equals(action)) {
            	return actionPreview(args, callbackContext);
            }
            
            if(AppivityFotoVideoPlugin.ACTION_VIDEO_START.equals(action)) {
            	return actionVideoStart(callbackContext);
            }

            if(AppivityFotoVideoPlugin.ACTION_VIDEO_STOP.equals(action)) {
                return actionVideoStop(callbackContext);
            }

            if(AppivityFotoVideoPlugin.ACTION_FOTO.equals(action)) {
            	return actionFoto(callbackContext);
            }
            
            callbackContext.error(TAG + ": INVALID ACTION");
            return false;
        } catch(Exception e) {
            Log.e(TAG, "ERROR: " + e.getMessage(), e);
            callbackContext.error(TAG + ": " + e.getMessage());
            return false;
        }
    }

    private boolean actionPreview(JSONArray args, final CallbackContext callbackContext) throws JSONException {
        String camerFace = args.getString(0);
        fileName = args.getString(1);
        
        if(videoOverlay == null) {
            Log.d(TAG, "Initializing camera");
            videoOverlay = new VideoOverlay(cordova.getActivity());
            videoOverlay.setCameraFacing(camerFace);

            if(videoOverlay.getViewType() == PreviewType.TEXTURE_VIEW) {
                relativeLayout = new RelativeLayout(cordova.getActivity());
            }

            //Get screen dimensions
            DisplayMetrics displaymetrics = new DisplayMetrics();
            cordova.getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            final int height = displaymetrics.heightPixels;
            final int width = displaymetrics.widthPixels;

            cordova.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    cordova.getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                    try {
                        if(videoOverlay.getViewType() == PreviewType.TEXTURE_VIEW) {
                            relativeLayout.addView(videoOverlay, new ViewGroup.LayoutParams(width, height));
                            cordova.getActivity().addContentView(relativeLayout, 
                            		new ViewGroup.LayoutParams(width, height));
                        } else {
                            // Set to 1 because we cannot have a transparent surface view, therefore 
                        	// view is not shown / tiny.
                            cordova.getActivity().addContentView(videoOverlay, 
                            		new ViewGroup.LayoutParams(1, 1));
                        }
                        Log.d(TAG, "IDZIE!!");
                        ViewGroup parent = (ViewGroup) webView.getView().getParent();
                        parent.removeView(webView.getView());
                        webView.getView().setBackgroundColor(0x00000000);
                        RelativeLayout webViewRelative = new RelativeLayout(cordova.getActivity());
                        relativeLayout.addView(webView.getView(), new ViewGroup.LayoutParams(width, height));
                        cordova.getActivity().addContentView(webViewRelative, new ViewGroup.LayoutParams(width, 
                        		height));
                        relativeLayout.bringToFront();
                        callbackContext.success(TAG + ": success" );
                    } catch(Exception e) {
                        Log.e(TAG, "Error during preview create", e);
                        callbackContext.error(TAG + ": " + e.getMessage());
                    }
                }
            });
        } else {
            Log.d(TAG, "Changing camera face");
            videoOverlay.setCameraFacing(camerFace);
            
            cordova.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        videoOverlay.stopPreview();
                        videoOverlay.startPreview();
                        callbackContext.success(TAG + ": success" );
                    } catch(Exception e) {
                        Log.e(TAG, "Error during preview create", e);
                        callbackContext.error(TAG + ": " + e.getMessage());
                    }
                }
            });
        }
    	return true;
    }
    
    private boolean actionVideoStart(final CallbackContext callbackContext) {
        if(videoOverlay != null) {
            cordova.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                	boolean recordingStarted = false;
                    if(videoOverlay != null)
                    	recordingStarted = videoOverlay.startRecording(getFilePath());
                    if (recordingStarted == true) {
                    	callbackContext.success("success");
                    } else {
                    	callbackContext.error("Camera error. Check android logcat for more information");
                    }
                }
            });
        } else {
        	callbackContext.error("Start action: Call before init");
        }
        return true;
    }
    
    private boolean actionVideoStop(final CallbackContext callbackContext) {
    	if(videoOverlay != null) {
            cordova.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(videoOverlay != null)
                    	try {
                    		videoOverlay.stopRecording();
                        	String filePathTmp = videoOverlay.actVideoPath;
                    		callbackContext.success(filePathTmp);
                    		Log.d(TAG, "Stop recording: " + filePathTmp);
                    	} catch (IOException e) {
                    		callbackContext.error("Stop recording error: " + e.getMessage());
                    	}
                }
            });
        } else {
        	callbackContext.error("Stop action: Call before init");
        }
        return true;
    }
    
    private boolean actionFoto(final CallbackContext callbackContext) {
        if(videoOverlay != null) {
            cordova.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(videoOverlay != null)
                    	videoOverlay.takeFoto(getImageFilePath(), callbackContext);
                    else
                    	callbackContext.error("Foto action: Call before init");
                }
            });
        } else {
        	callbackContext.error("Foto action: Call before init");
        }
        return true;
    }
    
    /* helpers */
    private String getFilePath(){
        return  filePath + getNextFileName() + FILE_EXTENSION;
    }

    private String getNextFileName(){
        int i = 1;
        String tmpFileName = fileName;
        while(new File(filePath + tmpFileName + FILE_EXTENSION).exists()) {
            tmpFileName = fileName + '_' + i;
            i++;
        }
        return tmpFileName;
    }

    private String getImageFilePath(){
        return  filePath + getNextImageFileName() + IMAGE_FILE_EXTENSION;
    }
    
    private String getNextImageFileName(){
        int i = 1;
        String tmpFileName = fileName;
        while(new File(filePath + tmpFileName + IMAGE_FILE_EXTENSION).exists()) {
            tmpFileName = fileName + '_' + i;
            i++;
        }
        return tmpFileName;
    }

    //Plugin Method Overrides
    @Override
    public void onPause(boolean multitasking) {
        if(videoOverlay != null)
            videoOverlay.onPause();
        super.onPause(multitasking);
    }

    @Override
    public void onResume(boolean multitasking) {
        super.onResume(multitasking);
        if(videoOverlay != null)
            videoOverlay.onResume();
    }

    @Override
    public void onDestroy() {
        if(videoOverlay != null)
            videoOverlay.onDestroy();
        super.onDestroy();
    }

}
