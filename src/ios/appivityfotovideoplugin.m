#import "appivityfotovideoplugin.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>

@implementation appivityfotovideoplugin

@synthesize parentView, view, session, output, outputPath, isFinished, previewLayer;

#ifndef __IPHONE_3_0
@synthesize webView;
#endif

- (CDVPlugin*) initWithWebView:(UIWebView*)theWebView {
    self = (appivityfotovideoplugin*)[super initWithWebView:theWebView];
    self.isReloaded = false;
    return self;
}

#pragma mark -
#pragma mark appivityfotovideoplugin

// action mappers
- (void) PREVIEW:(CDVInvokedUrlCommand *)command {
    [self initPreview: command];
}

- (void) VIDEO_START:(CDVInvokedUrlCommand *)command {
    [self startVideo: command];
}

- (void) VIDEO_STOP:(CDVInvokedUrlCommand *)command {
    [self stopVideo: command];
}

- (void) FOTO:(CDVInvokedUrlCommand *)command {
    [self foto: command];
}

- (void) SESSION_START:(CDVInvokedUrlCommand *)command {
    [self stopStartSession: command stop:false];
}

- (void) SESSION_STOP:(CDVInvokedUrlCommand *)command {
    [self stopStartSession: command stop:true];
}

// methods
- (void) initPreview:(CDVInvokedUrlCommand *)command {
    NSLog(@"DEBUG: Preview action");
    //stop the device from being able to sleep
    [UIApplication sharedApplication].idleTimerDisabled = YES;

    [self.webView setBackgroundColor: [UIColor clearColor]];
    self.webView.opaque = NO;
    
    self.camera = [command.arguments objectAtIndex:0];
    self.fileName = [command.arguments objectAtIndex:1];
    
    //get rid of the old dumb view (causes issues if the app is resumed)
    self.parentView = nil;
    
    //make the view
    int lWidth = self.webView.superview.frame.size.width;
    int lHeight = self.webView.superview.frame.size.height;
    CGRect viewRect = CGRectMake(0, 0, lWidth, lHeight);
    self.parentView = [[UIView alloc] initWithFrame: viewRect];
    [self.webView.superview addSubview:self.parentView];
    
    self.parentView.backgroundColor = [UIColor clearColor];
    self.view = [[UIView alloc] initWithFrame: self.parentView.bounds];
    [self.parentView addSubview: view];
    self.parentView.userInteractionEnabled = NO;
    
    // config session
    [self configureSession: false];
    
    //preview view
    self.previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:session];
    [self.previewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    
    CALayer *rootLayer = [[self view] layer];
    [rootLayer setMasksToBounds:YES];
    [rootLayer setBounds: viewRect];
    
    [self.previewLayer setFrame:CGRectMake(0, 0, lWidth, lHeight)];
    [rootLayer insertSublayer: self.previewLayer atIndex:-1];
    [self.viewController.view bringSubviewToFront:self.webView];
    [self.session startRunning];
    
    self.isBusy = false;
    //return true to ensure callback fires
    CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus: CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult: pluginResult callbackId:command.callbackId];
    
    // webView setBackground doesnt work correctly (transparecy will be updated after camera use)
    // setNeedsDisplay doesnt work, webView reload after first use do the job done
    if (self.isReloaded == false) {
        [self.webView reload];
        self.isReloaded = true;
    }
}

- (void) startVideo:(CDVInvokedUrlCommand *)command {
    NSLog(@"DEBUG: Video start action");
    if ([self isDeviceBusy] == true) {
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                          messageAsString:@"Cannot start recording - device is busy"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        return;
    }
    self.isBusy = true;
    //write the file
    outputPath = [self getVideoFileName];
    NSURL *fileURI = [[NSURL alloc] initFileURLWithPath:outputPath];
    //go
    [output startRecordingToOutputFileURL:fileURI recordingDelegate:self ];
    CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@" success"];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)stopVideo:(CDVInvokedUrlCommand *)command {
    NSLog(@"DEBUG: Video stop action");
    [output stopRecording];
    self.isBusy = false;
    CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:outputPath];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) foto:(CDVInvokedUrlCommand *)command {
    NSLog(@"DEBUG: Foto action");
    if ([self isDeviceBusy]) {
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                          messageAsString: @"Cannot take picture - device is busy"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        return;
    }
    self.isBusy = true;
    self.imageOutputPath = [self getImageFileName];
    AVCaptureConnection *videoConnection = [self getImageOutputConnection];
    
    [self.imageOutput captureStillImageAsynchronouslyFromConnection:videoConnection completionHandler: ^(CMSampleBufferRef imageSampleBuffer, NSError *error) {
        NSLog(@"DEBUG: Picture taken");
        NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation: imageSampleBuffer];
        //UIImage *image = [[UIImage alloc] initWithData:imageData];
        //UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
        NSFileManager *fileManager = [NSFileManager defaultManager];
        [fileManager createFileAtPath: self.imageOutputPath contents:imageData attributes:nil];
        
        self.isBusy = false;
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString: self.imageOutputPath];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void)stopStartSession:(CDVInvokedUrlCommand *)command stop:(Boolean)stop {
    if ([self isDeviceBusy]) {
        return;
    }
    if (stop) {
        NSLog(@"DEBUG: Stopping session");
        [self.session stopRunning];
    } else {
        NSLog(@"DEBUG: Startting session");
        [self.session startRunning];
    }
    self.isBusy = false;
    CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"success"];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (Boolean) isDeviceBusy {
    if (self.isBusy) {
        NSLog(@"DEBUG: Device is busy");
        return true;
    }
    return false;
}

- (AVCaptureConnection *) getImageOutputConnection {
    AVCaptureConnection *videoConnection = nil;
    for (AVCaptureConnection *connection in self.imageOutput.connections) {
        for (AVCaptureInputPort *port in [connection inputPorts]) {
            if ([[port mediaType] isEqual:AVMediaTypeVideo] ) {
                videoConnection = connection;
                break;
            }
        }
        if (videoConnection) { break; }
    }
    return videoConnection;
}

- (NSString*) getVideoFileName {
    return [self getFileName: FILE_EXTENSION];
}

- (NSString*) getImageFileName {
    return [self getFileName: IMAGE_FILE_EXTENSION];
}

- (NSString*)getFileName: (NSString *) fileExtension {
    int fileNameIncrementer = 1;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *libPath = [self getLibraryPath];
    
    NSString *tempPath = [[NSString alloc] initWithFormat:@"%@%@_%i%@", libPath, self.fileName, fileNameIncrementer, fileExtension];
    
    while ([fileManager fileExistsAtPath:tempPath]) {
        tempPath = [NSString stringWithFormat:@"%@%@_%i%@", libPath, self.fileName, fileNameIncrementer, fileExtension];
        fileNameIncrementer++;
    }
    return tempPath;
}

- (NSString*)getLibraryPath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *libraryPath = [paths objectAtIndex:0];
    return [NSString stringWithFormat:@"%@/", libraryPath];
}

- (AVCaptureDevice *)getCamera: (NSString *)camera {
    NSArray *videoDevices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    AVCaptureDevice *captureDevice = nil;
    for (AVCaptureDevice *device in videoDevices) {
        if([camera caseInsensitiveCompare:CAMERA_FRONT] == NSOrderedSame) {
            if (device.position == AVCaptureDevicePositionFront ) {
                captureDevice = device;
                break;
            }
        } else if ([camera caseInsensitiveCompare:CAMERA_BACK] == NSOrderedSame) {
            if (device.position == AVCaptureDevicePositionBack ) {
                captureDevice = device;
                break;
            }
        } else {
            //TODO: return cordova error
            NSLog(@"Coudn't find camera");
        }
    }
    return captureDevice;
}

- (void)configureSession: (Boolean) isPicture {
    self.session = [[AVCaptureSession alloc] init];
    if (isPicture == true) {
        [self.session setSessionPreset: AVCaptureSessionPresetPhoto];
    } else {
        [self.session setSessionPreset: AVCaptureSessionPresetHigh];
    }
    // config device
    self.inputDevice = [self getCamera: self.camera];
    
    //Capture audio input
    self.audioCaptureDevice = [AVCaptureDevice defaultDeviceWithMediaType: AVMediaTypeAudio];
    self.audioInput = [AVCaptureDeviceInput deviceInputWithDevice: self.audioCaptureDevice error:nil];
    
    //Capture device input
    self.deviceInput = [AVCaptureDeviceInput deviceInputWithDevice: self.inputDevice error:nil];
    
    if ([self.session canAddInput: self.audioInput]) {
        [self.session addInput: self.audioInput];
    } else {
        NSLog(@"DEBUG: Cannot add audio input");
    }
    
    if ([self.session canAddInput: self.deviceInput]) {
        [self.session addInput: self.deviceInput];
    } else {
        NSLog(@"DEBUG: Cannot add video input");
    }
    
    // add video device
    CMTime maxDuration = CMTimeMakeWithSeconds(1800, 1);
    output = [[AVCaptureMovieFileOutput alloc] init];
    output.maxRecordedDuration = maxDuration;
    
    if ([self.session canAddOutput:output]) {
        [self.session addOutput:output];
        NSLog(@"DEBUG: Output added");
    } else {
        NSLog(@"DEBUG: Cannot add outpu");
    }
    
    // add foto output
    self.imageOutput = [[AVCaptureStillImageOutput alloc] init];
    NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys: AVVideoCodecJPEG, AVVideoCodecKey, nil];
    [self.imageOutput setOutputSettings:outputSettings];
    
    if ([self.session canAddOutput: self.imageOutput]) {
        [self.session addOutput: self.imageOutput];
        NSLog(@"DEBUG: Image output added");
    } else {
        NSLog(@"DEBUG: Cannot add image output");
    }
}

- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didFinishRecordingToOutputFileAtURL:(NSURL *)outputFileURL fromConnections:(NSArray *)connections error:(NSError *)error {
}

@end