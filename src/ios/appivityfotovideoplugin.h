#import <UIKit/UIKit.h>

#import <Cordova/CDVPlugin.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>

#define FILE_EXTENSION @".mp4"
#define IMAGE_FILE_EXTENSION @".jpg"
#define CAMERA_FRONT @"CAMERA_FRONT"
#define CAMERA_BACK @"CAMERA_BACK"

@interface appivityfotovideoplugin : CDVPlugin <UITabBarDelegate, AVCaptureFileOutputRecordingDelegate> {
}

@property AVCaptureVideoPreviewLayer *previewLayer;
@property (nonatomic, retain) UIView* parentView;
@property (nonatomic, retain) UIView* view;
@property AVCaptureDevice *inputDevice;
@property AVCaptureDevice *audioCaptureDevice;
@property AVCaptureDeviceInput *audioInput;
@property AVCaptureDeviceInput *deviceInput;
@property AVCaptureSession *session;
@property AVCaptureMovieFileOutput *output;
@property AVCaptureStillImageOutput *imageOutput;
@property NSString *outputPath;
@property NSString *imageOutputPath;
@property Boolean isReloaded;
@property Boolean isBusy;
@property NSString *fileName;
@property (assign) BOOL isFinished;
@property NSString *camera;

// actions mapper
- (void)PREVIEW:(CDVInvokedUrlCommand *)command;
- (void)VIDEO_START:(CDVInvokedUrlCommand *)command;
- (void)VIDEO_STOP:(CDVInvokedUrlCommand *)command;
- (void)FOTO:(CDVInvokedUrlCommand *)command;
- (void)SESSION_STOP:(CDVInvokedUrlCommand *)command;
- (void)SESSION_START:(CDVInvokedUrlCommand *)command;

@end