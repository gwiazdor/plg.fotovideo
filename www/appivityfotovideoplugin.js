//cordova.define("pl.appivity.fotovideoplugin.appivityfotovideoplugin", function(require, exports, module) { 
var cordova = require('cordova');

var appivityfotovideoplugin = {
    ACTION_PREVIEW: 'PREVIEW',
    ACTION_VIDEO_START: 'VIDEO_START',
    ACTION_VIDEO_STOP: 'VIDEO_STOP',
    ACTION_FOTO: 'FOTO',
    ACTION_SESSION_STOP: 'SESSION_STOP', // tylko w implementacji iOS
    ACTION_SESSION_START: 'SESSION_START', // tylko w implementacji iOS
    CAMERA_FRONT: 'CAMERA_FRONT',
    CAMERA_BACK: 'CAMERA_BACK',

    init: function(camera, fileName, successFunction, errorFunction) {
        camera = camera || this.CAMERA_BACK;
        cordova.exec(successFunction, errorFunction, 'appivityfotovideoplugin', this.ACTION_PREVIEW,
            [camera, fileName]);
    },
    startVideo : function(successFunction, errorFunction) {
    	cordova.exec(successFunction, errorFunction, 'appivityfotovideoplugin', this.ACTION_VIDEO_START, []);
    },
    stopVideo : function(successFunction, errorFunction) {
        cordova.exec(successFunction, errorFunction, 'appivityfotovideoplugin', this.ACTION_VIDEO_STOP, []);
    },
    foto: function(successFunction, errorFunction) {
        cordova.exec(successFunction, errorFunction, 'appivityfotovideoplugin', this.ACTION_FOTO, []);
    },
    sessionStop: function(successFunction, errorFunction) {
        cordova.exec(successFunction, errorFunction, 'appivityfotovideoplugin', this.ACTION_SESSION_STOP, []);
    },
    sessionStart: function(successFunction, errorFunction) {
        cordova.exec(successFunction, errorFunction, 'appivityfotovideoplugin', this.ACTION_SESSION_START, []);
    }
};

module.exports = appivityfotovideoplugin;
window.Plugin.appivityfotovideoplugin = appivityfotovideoplugin;

//});
