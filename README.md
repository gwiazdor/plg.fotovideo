Projekt bazowy : backgroundvideo

# Interfejs JS:

##Funkcja inicjalizująca podgląd
#####camera (String): wybór kamery, przednia CAMER_FRONT, tylna CAMERA_BACK
#####fileName (String): prefix dla nazw plików zapisywanych na urządzeniu
#####successFunction (function): funkcja wywoływana w przypadku powodzenia
#####errorFunction (funcion): funkcja wywoływana w przypadku niepowodzenia
```
init(camera, fileName, successFunction, errorFunction)
```

##Funkcja rozpoczynająca nagrywanie filmu
#####successFunction (function): funkcja wywoływana w przypadku powodzenia
#####errorFunction (funcion): funkcja wywoływana w przypadku niepowodzenia
```
startVideo(successFunction, errorFunction)
```

##Funkcja kończąca nagrywanie filmu
#####successFunction (function): funkcja wywoływana w przypadku powodzenia
#####errorFunction (funcion): funkcja wywoływana w przypadku niepowodzenia
```
stopVideo(successFunction, errorFunction)
```

##Funkcja robienia zdjęcia
#####successFunction (function): funkcja wywoływana w przypadku powodzenia
#####errorFunction (funcion): funkcja wywoływana w przypadku niepowodzenia
```
foto(successFunction, errorFunction)
```
